"""
Objects are distinguished from functions in that it may have useful properties or functions
as attributes after the initial call.

A standard init file is used from src.
"""
from os import path
src_init_path = path.abspath(path.join(path.dirname(path.realpath(__file__)), '..', '__init__namespace.py'))
exec(open(src_init_path).read())