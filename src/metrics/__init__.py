"""
Metrics are functions that return a float between 0 and 1.

A standard init file is used from src.
"""
from os import path
src_init_path = path.abspath(path.join(path.dirname(path.realpath(__file__)), '..', '__init__namespace.py'))
exec(open(src_init_path).read())