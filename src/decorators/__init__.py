"""
Decorators are functions that receive a function or object as the first argument.

A standard init file is used from src.
"""
from os import path
src_init_path = path.abspath(path.join(path.dirname(path.realpath(__file__)), '..', '__init__namespace.py'))
exec(open(src_init_path).read())