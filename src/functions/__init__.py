"""
Functions require only one call to execute, as opposed to objects, which first require instantiation.
Import from functions as a direct subdirectory of the root repository module or add TOOLS_REPO/src to PYTHONPATH

A standard init file is used from src.
"""
from os import path
src_init_path = path.abspath(path.join(path.dirname(path.realpath(__file__)), '..', '__init__namespace.py'))
exec(open(src_init_path).read())