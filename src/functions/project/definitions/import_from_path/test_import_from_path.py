from import_from_path import import_from_path
import types
import git

def test_import_from_path():
    repo = git.Repo(__file__, search_parent_directories=True)
    repo_root = repo.working_tree_dir
    func = import_from_path(f'{repo_root}/src/functions/project/definitions/define/define.py')
    assert type(func) == types.ModuleType