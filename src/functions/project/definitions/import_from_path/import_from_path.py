import os
from importlib.util import spec_from_file_location, module_from_spec

def import_from_path(path: str):
    """ Given a filepath, return the module """
    name = os.path.basename(path).replace('.py', '')
    mod = None
    print('name', name)
    print('path', path)
    try:
        spec = spec_from_file_location(name, path)
        mod = module_from_spec(spec)
        spec.loader.exec_module(mod)
    except ModuleNotFoundError:
        pass
    return mod
