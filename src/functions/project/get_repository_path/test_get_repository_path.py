from get_repository_path import get_repository_path
import os

def test_get_repository_path():
    assert "src" in os.listdir(get_repository_path())
