""" Command line utility for repository """
import fire
import functions, objects, metrics, decorators


def command_line():
    """ Command-line interface for the repository
    Specify the definition to execute and then any arguments
    e.g. "functions define". 
    The Fire library converts the specified function or object into a command-line utility.
    """
    definitions = {
        'functions': functions,
        'objects': objects,
        'metrics': metrics,
        'decorators': decorators
    }

    fire.Fire(definitions)
