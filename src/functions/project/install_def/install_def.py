from functions import find_definition
from os.path import join, isfile
import subprocess
import sys

def install_def(def_name: str, install=True):
    """ Install using install.sh and requirements.txt 
    at definition folder
    If install is False, install.sh won't be run """
    def_path = find_definition(def_name)
    if install:
        install_filepath = join(def_path, 'install.sh')
        if isfile(install_filepath):
            try:
                subprocess.check_output(['sh', install_filepath])
            except subprocess.CalledProcessError as e:
                print(e.output, file=sys.stderr)
                return False
    reqs_filepath = join(def_path, 'requirements.txt')
    if isfile(reqs_filepath):
        try:
            subprocess.check_output(['pip', 'install', '-r', reqs_filepath, shell=True, stdout=subprocess.STDOUT])
        except subprocess.CalledProcessError as e:
            print(e.output, file=sys.stderr)
            return False
    return True
