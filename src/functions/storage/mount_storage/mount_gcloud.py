import os
from google.colab import auth

def mount_gcloud(bucket_name):
    """ Mount Google Cloud storage bucket """
    os.system('echo "deb http://packages.cloud.google.com/apt gcsfuse-`lsb_release -c -s` main" | sudo tee /etc/apt/sources.list.d/gcsfuse.list')
    os.system('curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -')
    os.system('sudo apt-get -y -q update')
    os.system('sudo apt-get -y -q install gcsfuse')
    auth.authenticate_user()
    os.system('mkdir -p {}'.format(bucket_name))
    os.system('gcsfuse --implicit-dirs --limit-bytes-per-sec -1 --limit-ops-per-sec -1 {} {}'.format(bucket_name, bucket_name))