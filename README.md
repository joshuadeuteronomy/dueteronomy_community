# utils
Reusable code or "utilities" with definitions (interfaces) and implementations.
While this repository only supports Python interfaces currently, implementations of interfaces can be done in any language.

## Installation
`pip install -e .`
Remove -e for production or if you won't be making any changes to the repository code.

## Updating dependencies
`pip install -r requirements.txt`

## Goals
There are two principle goals for the organization of this repository:

- Reusability - Code is findable, understood, and useable in multiple contexts
- Experimentation - All interfaces can be implemented in multiple ways, and these implementations can be compared.

## Folders
- src - For code interfaces and their implementations
- src/objects - Classes, defined as code that requires instantiation before running (two calls)
- src/functions - Functions, defined as code that doesn't require instantiation to run (single call)
- src/metrics - For comparing multiple implementations of an interface.
- src/decorators - Used to alter the functionality of functions or objects.
- data - Standard data or test data, separated into folders
- notebooks - For ipynb files that can be run in a Jupyter notebook

### Note on test files
- Instead of a separate tests folder, it's recommended to put a file starting with test_ in the same directory as the module being tested.

## Folders as categories
To start, code should be under at least one subfolder of src/objects or src/functions, and data a subfolder under data. This encourages reasonable organization. Folder location can be changed later, since file path isn't needed for importing.

## Category labels
It's recommended to specify a list of `categories` at the top of in every code file. Example:
`categories = ['recommendation', 'summarization']`
This allows searching for code by category, and means a single module can have multiple categories. Each parent folder under src will be included as a category automatically.

## Importing
Regardless of file location, importing works as such:
`from utils.functions import summarize`
By adding the src directory to your Python path, you can import as such:
`from functions import summarize`
The latter is how imports are done throughout the repository. Each of the top-level directories under src are considered namespaces.

## Definitions and Implementations
Every object and function in this repository is considered an interface, also called a solution definition, which could have many alternative implementations. This doesn't require coding any differently except when choosing to do the below. A function or object can be converted into an interface at any time without affecting other code that depends on it.

To define an interface, create a folder with the name to be imported, and then create a file inside it with the same name. This serves as the interface. Then either in the same file or as separate files, create implementations. These can even be imported from subfolders.

Implementations aren't intended to be publicly accessible except by the interface. To use a specific implementation, set the `version` parameter of the interface after importing.

Unit tests are generally for the defined interface, and these should be put in the same folder as the interface. There can also be unit tests for specific implementations as needed. Test filenames start with "test_".

Metrics are placed in a separate metrics folder, since metrics may apply to multiple definitions. Metrics themselves have interfaces and implementations. However, metrics return a single number.

For those definitions that have associated metrics, the implementation used for that definition will be the one with the highest metric scores.

## Command Line Interface
The standard command line interface for the repository is defined at src/functions/project/command_line. All definitions are automatically useable by the command line, and this command line interface is available after installation (by executing the name of the repository).

Usage:
`utils <namespace> <definition name>`

Examples:
`utils functions find_definition define`
`utils functions test_def define`

Here are recommended aliases for your .bashrc file for convenience:
alias def="utils functions define"
alias test="utils functions test_def"
alias finddef="utils functions find_definition"
alias mvdef="utils functions rename_def"